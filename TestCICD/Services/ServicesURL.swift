//
//  ServicesURL.swift
//  TestCICD
//
//  Created by t19960804 on 5/3/21.
//

import Foundation

public struct ServicesURL {
    
    static var baseurl: String {
        return (Bundle.main.infoDictionary?["API_URL"] as? String)?.replacingOccurrences(of: "\\", with: "") ?? ""
    }
}
